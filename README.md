Fall 2016 course project for CS 248A: Introduction to Ubiquitous Computing.

The goal of our project was to create a way to aid the visually impaired in indoor navigation. We wanted users to be able to attach a device to their white cane that would detect nearby RFID tags and provide feedback to the user (through their mobile phones) regarding their current location and help them navigate indoor structures. We also considered how this could be applicable for users to navigate and interact with museum displays or other indoor objects. Services that use GPS are typically only good for navigating outdoors and have weak signals indoors. 

The prototype navigates the user within a lecture hall at our school. (Arduino module setup and code not included).

I ...
* Teamed up with three developers to create an application for indoor navigation to guide the visually impaired
* Prototyped a proof-of-concept in Android to provide instructions, directions, and audio feedback to the user
* Tested the appplication to communicate with an Arduino Bluetooth module attached to a white cane