package com.example.talkingcane;

import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainMenu extends Activity {
	
	private TextToSpeech tts;
	private TextToSpeech ttsLocation;
	private String speechWelcome;
	private String speechFeedback;
	private String speechLocation;
	boolean buildingFound = false;
	
	private EditText building;
	private Button searchButton;
	private DbAdapter db;
	private Cursor cur;

	private String buildingQuery;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
        
		TextView welcomeText = (TextView)this.findViewById(R.id.welcome);
		speechWelcome = welcomeText.getText().toString();
		TextView feedbackText = (TextView)this.findViewById(R.id.feedback);
		speechFeedback = feedbackText.getText().toString();
		
		tts = new TextToSpeech(MainMenu.this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if(status == TextToSpeech.SUCCESS){
                    tts.setLanguage(Locale.US);
                    
                   	tts.speak(speechWelcome, TextToSpeech.QUEUE_FLUSH, null);
                   	tts.playSilence(500, TextToSpeech.QUEUE_ADD, null);
                   	tts.speak(speechFeedback, TextToSpeech.QUEUE_ADD, null);
                }
            }
        });
		
   		TextView debugText = (TextView)this.findViewById(R.id.debugtext);
   		debugText.setText("");

   		building = (EditText)this.findViewById(R.id.search_text);

		this.searchButton = (Button)this.findViewById(R.id.do_search);
		this.searchButton.setOnClickListener(new OnClickListener() {
		   	public void onClick(View v) {
		   		
		   		
		   		buildingQuery = building.getText().toString();
		   		
//		   		test(buildingQuery);

		   		int buildingID = findBuilding(buildingQuery);
		   		String buildingLayout = getLayout(buildingQuery);
		   		float buildingRotation = getRotation(buildingQuery);
		   		
	    		if(buildingID != -1)
	    		{
	    			Intent intent = new Intent(MainMenu.this, Navigation.class); //intent for specific component
	    			
		    		intent.putExtra("buildingID", buildingID);
		    		intent.putExtra("buildingLayout", buildingLayout);
		    		intent.putExtra("buildingRotation", buildingRotation);

	    			startActivity(intent);

	    		}
	    	}
	    });

	}
	
	public void test(String text)
	{
   		TextView debugText = (TextView)this.findViewById(R.id.debugtext);

        debugText.setText("Input was... " + text);
	}
	
	public int findBuilding(String input)
	{
//        TextView debugText = (TextView)this.findViewById(R.id.debugtext);
        TextView feedback = (TextView)this.findViewById(R.id.feedback);

        db = new DbAdapter(this);
		String result = "Loading " + input + " ";
		int buildingID = -1;
		
//		cur = db.getBuildingIDFromAbbr(input);		
		cur = db.getBuildingID(input.trim());
		
		if(cur.moveToFirst())
		{
			buildingID = cur.getInt(cur.getColumnIndex("id"));
			speechLocation = input;
			buildingFound = true;
		}
		else
		{
			result = "Building not found.";
			speechLocation = result;
			buildingFound = false;
		}
        
//		feedback.setText(result);
		
		ttsLocation = new TextToSpeech(MainMenu.this, new TextToSpeech.OnInitListener() {
		    @Override
		    public void onInit(int status) {
		        // TODO Auto-generated method stub
		        if(status == TextToSpeech.SUCCESS){
		        	ttsLocation.setLanguage(Locale.US);
		            
		        	if(buildingFound)
		        		ttsLocation.speak("You are in " + speechLocation, TextToSpeech.QUEUE_FLUSH, null);
		        	else
		        		ttsLocation.speak(speechLocation, TextToSpeech.QUEUE_FLUSH, null);
		        	ttsLocation.playSilence(500, TextToSpeech.QUEUE_ADD, null);
		        }
		    }
		});

		
//        debugText.setText(result);

        return buildingID;
	}
	
	public String getLayout(String input)
	{
//        TextView debugText = (TextView)this.findViewById(R.id.debugtext);
        
        db = new DbAdapter(this);
		String layout = "none";
		
		cur = db.getBuildingLayout(input.trim());
		
		if(cur.moveToFirst())
			layout = cur.getString(cur.getColumnIndex("layout"));
		else
			layout = "None.";

//        debugText.setText(layout);

        return layout;
	}

	public float getRotation(String input)
	{
        TextView debugText = (TextView)this.findViewById(R.id.debugtext);
        
        db = new DbAdapter(this);
		float degrees = 0;
		
		cur = db.getBuildingRotation(input.trim());
		
		if(cur.moveToFirst())
			degrees = cur.getFloat(cur.getColumnIndex("map_rotation"));
		else
			degrees = 0;
        		
        debugText.setText("");

        return degrees;
	}

}
