package com.example.talkingcane;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class Navigation extends Activity implements SensorEventListener {
	
	private ImageView mLoadMap;
	
	private ImageView mPointer;
	private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;
    private float[] mLastAccelerometer = new float[3];
    private float[] mLastMagnetometer = new float[3];
    private boolean mLastAccelerometerSet = false;
    private boolean mLastMagnetometerSet = false;
    private float[] mR = new float[9];
    private float[] mOrientation = new float[3];
    private float mCurrentDegree = 0f;
    private float buildingRotation = 0;
    
	private TextToSpeech tts;
	private TextToSpeech ttsToLocation;
	private TextToSpeech ttsFromLocation;
	private String speech;
    
    EditText destination;
    Button searchButton;
    String destinationQuery;
	private DbAdapter db;
	private Cursor cur;
	
//    public static EditText currentLoc;
    public static TextView currentLoc;
	Button rfidSample;
	String rfidQuery;
	
	String fromLocationName = "null";
	String toLocationID = "null";
	
	Button navButton;
	String where_face = "null";
	boolean correctDirection;

	boolean stopFace = false;
	
	private Handler mHandler = new Handler();
	private BroadcastReceiver receiver;

	static boolean active = false;
	static Navigation navigation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_navigation);

		active = true;
		navigation = this;

		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
	    mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    mPointer = (ImageView) findViewById(R.id.pointer);
	    
	    mLoadMap = (ImageView) findViewById(R.id.loadmap);

	    buildingRotation = this.getIntent().getExtras().getFloat("buildingRotation");
	    String filename = this.getIntent().getExtras().getString("buildingLayout");
	        
	    int resID = getResources().getIdentifier(filename, "drawable", getPackageName());
	    mLoadMap.setImageResource(resID);
	    
   		destination = (EditText)this.findViewById(R.id.search_text);
   		currentLoc = (TextView)this.findViewById(R.id.getloctext);

   		updateDebug("");
   		
		TextView feedbackText = (TextView)this.findViewById(R.id.feedback);
		speech = feedbackText.getText().toString();
		
		tts = new TextToSpeech(Navigation.this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                // TODO Auto-generated method stub
                if(status == TextToSpeech.SUCCESS){
                    int result=tts.setLanguage(Locale.US);
                    if(result==TextToSpeech.LANG_MISSING_DATA ||
                            result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e("error", "This Language is not supported");
                    }
                    else{
                    	tts.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
                    }
                }
                else
                    Log.e("error", "Initilization Failed!");
            }
        });
   		
		this.searchButton = (Button)this.findViewById(R.id.do_search);
		this.searchButton.setOnClickListener(new OnClickListener() {
		   	public void onClick(View v) {
		   		
		   		destinationQuery = destination.getText().toString();
		   		toLocationID = findLocationID(destinationQuery);

    			ttsToLocation = new TextToSpeech(Navigation.this, new TextToSpeech.OnInitListener() {
    			    @Override
    			    public void onInit(int status) {
    			        // TODO Auto-generated method stub
    			        if(status == TextToSpeech.SUCCESS){
    			        	ttsToLocation.setLanguage(Locale.US);
    			            
    			    		if(toLocationID.equals("null"))
    			    		{
    					   		String feedbackString = "Location was not found.";
    			    			updateFeedback(feedbackString);

    			        		ttsToLocation.speak(feedbackString, TextToSpeech.QUEUE_FLUSH, null);
    			    		}
    			    		else
    			    		{
//    			    			updateFeedback("Location ID = " + toLocationID);
    			    			String feedbackString = "\"" + destinationQuery + "\" was found. \nPlease tap an RFID to start navigation.";
    			    			updateFeedback(feedbackString);
    			    			
    			    			ttsToLocation.speak(feedbackString, TextToSpeech.QUEUE_FLUSH, null);
    			    		}
    			        }
    			    }
    			});
	    	}
	    });
		
		/*
		this.rfidSample = (Button)this.findViewById(R.id.getloc);
		this.rfidSample.setOnClickListener(new OnClickListener() {
		   	public void onClick(View v) {
		   		
		   		rfidQuery = currentLoc.getText().toString();
		   		fromLocationName = findLocationName(rfidQuery);
		   		
	    		if(fromLocationName.equals("null"))
	    		{
	    			updateFeedback("Location was not found.");
	    		}
	    		else
	    		{
	    			updateFeedback("You are currently at " + fromLocationName + ".");
	    		}
	    	}
	    });*/

		/*
		//TODO: replace nav button with start?  
		this.navButton = (Button)this.findViewById(R.id.navigate);
		this.navButton.setOnClickListener(new OnClickListener() {
		   	public void onClick(View v) {
		   		
		   		rfidQuery = currentLoc.getText().toString();
		   		fromLocationName = findLocationName(rfidQuery);
		   		
		   		destinationQuery = destination.getText().toString();
		   		toLocationID = findLocationID(destinationQuery);
		   		
	    		if(!fromLocationName.equals("null") && !toLocationID.equals("null"))
	    		{
	    			String fromID = rfidQuery.toUpperCase().trim();
	    			String toID = toLocationID.toUpperCase().trim();
	    			
	    			if(fromID.equals(toID))
	    			{
	    				updateFeedback("You're already here!");
	    				stopFace = true;
	    			}
	    			else
	    			{
		    			where_face = getWhereToFaceString(fromID, toID);
		    			if(!where_face.equals("null"))
		    			{
		    				stopFace = false;
			    			updateFeedback("Please face " + where_face + "\n with respect to the building.");
			    			
			    			faceDirection();
		    			}
		    			else
		    			{
		    				updateFeedback("Error. Cannot find a path.");
		    			}
		
	//	    			String directions = getDirectionsString(fromID, toID);
	//	    			updateFeedback(directions);
	    			}	    			
	    			//TODO: directions relative to map (map is not always north)
	    		}
	    		else
	    		{
	    			if(fromLocationName.equals("null"))
	    				updateFeedback("Error: Invalid starting point.");
	    			else if(toLocationID.equals("null"))
	    				updateFeedback("Error: Invalid destination.");
	    			else
	    				updateFeedback("Error: No information.");
	    			
	    		}
	    		
	    	}
	    });*/

	}
	
	public void updateFeedback(String output)
	{
		TextView feedback = (TextView)this.findViewById(R.id.feedback);
		feedback.setText(output);
	}
	
	public void updateDebug(String output)
	{
		TextView debugText = (TextView)this.findViewById(R.id.debugtext);
		debugText.setText(output);
	}
	
	public String getDirectionsString(String fromID, String toID)
	{
        db = new DbAdapter(this);

        fromID = fromID.toUpperCase().trim();
        toID = toID.toUpperCase().trim();
        
        cur = db.getDirections(fromID, toID);
		String locationName = "none";
		
		 TextView debugText = (TextView)this.findViewById(R.id.debugtext);
		 debugText.setText(fromID + " " + toID);
		
		if(cur.moveToFirst())
			locationName = cur.getString(cur.getColumnIndex("directions"));
		else
			locationName = "null";
		
      
		return locationName;
	}
	
	public String getWhereToFaceString(String fromID, String toID)
	{
        db = new DbAdapter(this);

        fromID = fromID.toUpperCase().trim();
        toID = toID.toUpperCase().trim();
        
        cur = db.getWhereToFace(fromID, toID);
		String directionToFace = "none";
		
//		 TextView debugText = (TextView)this.findViewById(R.id.debugtext);
//		 debugText.setText(fromID + " " + toID);
		
		if(cur.moveToFirst())
			directionToFace = cur.getString(cur.getColumnIndex("face"));
		else
			directionToFace = "null";
		
		return directionToFace;
	}
	
	public String findLocationName(String rfid)
	{
        db = new DbAdapter(this);

	    int buildingID = this.getIntent().getExtras().getInt("buildingID");
        cur = db.findLocNameInBuilding(rfid.trim(), buildingID);
		String locationName = "none";
		
		if(cur.moveToFirst())
			locationName = cur.getString(cur.getColumnIndex("location"));
		else
			locationName = "null";
		
      
		return locationName;
	}
	
	public String findLocationID(String input)
	{
        db = new DbAdapter(this);

	    int buildingID = this.getIntent().getExtras().getInt("buildingID");
        cur = db.findLocIDInBuilding(input.trim(), buildingID);
		String destinationID = "none";
		
		if(cur.moveToFirst())
			destinationID = cur.getString(cur.getColumnIndex("id"));
		else
			destinationID = "null";

		return destinationID;
	}
	
	public int getBuildingID()
	{
		int buildingID = this.getIntent().getExtras().getInt("buildingID");
		return buildingID;
	}
	
	public String getBuildingLayout()
	{
		String buildingLayout = this.getIntent().getExtras().getString("buildingLayout");
		return buildingLayout;
	}
	
	public String getBuildingRotation()
	{
		String buildingRotation = this.getIntent().getExtras().getString("buildingRotation");
		return buildingRotation;
	}
	
	//TODO: if tap rfid, compare name to destination instead of id
	//TODO: one room can have two rfid, so how does it choose which rfid is the destination?
	private void faceDirection(){
		
		correctDirection = false;
	
		runOnUiThread(new Runnable(){
			@Override
			public void run(){
				
			/* North = 0
			 * North East = 45
			 * East = 90
			 * South East = 135
			 * South = 180
			 * South West = 225
			 * West = 270
			 * North West = 315
			 */
		        mHandler.postDelayed(this, 100);
		        float actualDegree = mCurrentDegree + buildingRotation;

//				updateDebug(String.valueOf(mCurrentDegree));
	
//				if(where_face.equals("North"))
//				{	
//					if(mCurrentDegree >= 337.5 || mCurrentDegree < 22.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("North East"))
//				{
//					if(mCurrentDegree >= 22.5 && mCurrentDegree < 67.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("East"))
//				{
//					if(mCurrentDegree >= 67.5 && mCurrentDegree < 112.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("South East"))
//				{
//					if(mCurrentDegree >= 112.5 && mCurrentDegree < 157.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("South"))
//				{
//					if(mCurrentDegree >= 157.5 && mCurrentDegree < 202.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("South West"))
//				{
//					if(mCurrentDegree >= 202.5 && mCurrentDegree < 247.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("West"))
//				{
//					if(mCurrentDegree >= 247.5 && mCurrentDegree < 292.5)
//					{
//						correctDirection = true;
//					}
//				}
//				else if(where_face.equals("North West"))
//				{
//					if(mCurrentDegree >= 292.5 && mCurrentDegree < 337.5)
//					{
//						correctDirection = true;
//					}
//				}
		        
		        if(where_face.equals("North"))
				{	
					if(actualDegree >= 337.5 || actualDegree < 22.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("North East"))
				{
					if(actualDegree >= 22.5 && actualDegree < 67.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("East"))
				{
					if(actualDegree >= 67.5 && actualDegree < 112.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("South East"))
				{
					if(actualDegree >= 112.5 && actualDegree < 157.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("South"))
				{
					if(actualDegree >= 157.5 && actualDegree < 202.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("South West"))
				{
					if(actualDegree >= 202.5 && actualDegree < 247.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("West"))
				{
					if(actualDegree >= 247.5 && actualDegree < 292.5)
					{
						correctDirection = true;
					}
				}
				else if(where_face.equals("North West"))
				{
					if(actualDegree >= 292.5 && actualDegree < 337.5)
					{
						correctDirection = true;
					}
				}
				
				if(correctDirection)
				{
					//once we find the user is in the correct direction, stop this thread
					mHandler.removeCallbacks(this);
					
					String fromID = rfidQuery.toUpperCase().trim();
	    			String toID = toLocationID.toUpperCase().trim();
					
					Intent intent = new Intent(Navigation.this, Instructions.class);
					intent.putExtra("from_id", fromID);
					intent.putExtra("to_id", toID);
					
					int buildingID = getBuildingID();
					intent.putExtra("buildingID", buildingID);
					String buildingLayout = getBuildingLayout();
					intent.putExtra("buildingLayout", buildingLayout);
					String buildingRotation = getBuildingRotation();
					intent.putExtra("buildingRotation", buildingRotation);

					active = false;
					startActivity(intent);
					ttsFromLocation.shutdown();
					ttsToLocation.shutdown();
					finish();
				}
				
				if(stopFace)
				{
					mHandler.removeCallbacks(this);
				}
			}
		});
	}
	
	protected void onResume() {
	    super.onResume();
	    mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_GAME);

		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothActivity.EXTRA_DATA);

		receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Toast.makeText(getApplicationContext(), "received", Toast.LENGTH_SHORT).show();
			}
		};

		this.registerReceiver(receiver, filter);
		super.onResume();
	}
	 
	protected void onPause() {
	    super.onPause();
	    mSensorManager.unregisterListener(this, mAccelerometer);
        mSensorManager.unregisterListener(this, mMagnetometer);

		this.unregisterReceiver(receiver);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor == mAccelerometer) {
            System.arraycopy(event.values, 0, mLastAccelerometer, 0, event.values.length);
            mLastAccelerometerSet = true;
        } else if (event.sensor == mMagnetometer) {
            System.arraycopy(event.values, 0, mLastMagnetometer, 0, event.values.length);
            mLastMagnetometerSet = true;
        }
        if (mLastAccelerometerSet && mLastMagnetometerSet) {
            SensorManager.getRotationMatrix(mR, null, mLastAccelerometer, mLastMagnetometer);
            SensorManager.getOrientation(mR, mOrientation);
            float azimuthInRadians = mOrientation[0];
            float azimuthInDegrees = (float)(Math.toDegrees(azimuthInRadians)+360)%360;
            RotateAnimation ra = new RotateAnimation(
            		mCurrentDegree - buildingRotation, //fromDegrees
            		azimuthInDegrees - buildingRotation, //toDegrees
                    Animation.RELATIVE_TO_SELF, 0.5f, 
                    Animation.RELATIVE_TO_SELF, 0.5f
            );
     
//            TextView debugText = (TextView)this.findViewById(R.id.debugtext);
//            debugText.setText("az = " + azimuthInDegrees);
            
            ra.setDuration(250);
     
            ra.setFillAfter(true);
     
            mPointer.startAnimation(ra);
            mCurrentDegree = azimuthInDegrees;
        }
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public String rfidToString(String input)
	{
        //TextView debugText = (TextView)this.findViewById(R.id.debugtext);
        
        db = new DbAdapter(this);
		String result = "Loading...";
		String locName = "null";
		
		cur = db.getLocNameFromRFID(input.trim());
		
		if(cur.moveToFirst())
			locName = cur.getString(cur.getColumnIndex("location"));
		else
			locName = "Building not found.";
        
		//debugText.setText(locName);
		
        return locName;
	}
	

	//-------- Bluetooth code --------//


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

	}

	static void displayData(String data){		
		if (data != null) {

			final String data2 = data;
			final String locName = navigation.rfidToString(data);
			
			Handler refresh = new Handler(Looper.getMainLooper());
			refresh.post(new Runnable() {
				public void run()
				{ 
					//currentLoc.setText(data2);
					currentLoc.setText(locName);
					
					navigation.navigate(data2, locName);
				}
			});
		}

	}

	public void navigate(String tag, String locName){
		//rfidQuery = currentLoc.getText().toString();
		rfidQuery = tag;
//		fromLocationName = findLocationName(rfidQuery);
		fromLocationName = findLocationName(tag);

		destinationQuery = destination.getText().toString();
		toLocationID = findLocationID(destinationQuery);

		final String temp = locName;
		navigation.ttsFromLocation = new TextToSpeech(Navigation.this, new TextToSpeech.OnInitListener() {
		    @Override
		    public void onInit(int status) {
		        // TODO Auto-generated method stub
		        if(status == TextToSpeech.SUCCESS){
		        	navigation.ttsFromLocation.setLanguage(Locale.US);
		            
			   		String feedbackString = "Your current location is " + temp;
	    			navigation.ttsFromLocation.speak(feedbackString, TextToSpeech.QUEUE_FLUSH, null);
		
					if(!fromLocationName.equals("null") && !toLocationID.equals("null"))
					{
						String fromID = rfidQuery.toUpperCase().trim();
						String toID = toLocationID.toUpperCase().trim();

						if(fromID.equals(toID))
						{
							feedbackString = "You're already here!";
							updateFeedback(feedbackString);
			    			navigation.ttsFromLocation.speak(feedbackString, TextToSpeech.QUEUE_ADD, null);

							stopFace = true;
						}
						else
						{
							where_face = getWhereToFaceString(fromID, toID);

							if(!where_face.equals("null"))
							{
								stopFace = false;
								feedbackString = "Please face " + where_face + ".";
								updateFeedback(feedbackString);
								navigation.ttsFromLocation.speak(feedbackString, TextToSpeech.QUEUE_ADD, null);
			
								faceDirection();
							}
							else
							{
								feedbackString = "Error. Cannot find a path.";
								updateFeedback(feedbackString);
								navigation.ttsFromLocation.speak(feedbackString, TextToSpeech.QUEUE_ADD, null);
							}
			
							//	    			String directions = getDirectionsString(fromID, toID);
							//	    			updateFeedback(directions);
						}
						//TODO: directions relative to map (map is not always north)
					}
					else //for debugging
					{
						if(fromLocationName.equals("null"))
						{
							feedbackString = "You are in the wrong building!\nPlease tap another RFID.";
							updateFeedback(feedbackString);
							navigation.ttsFromLocation.speak(feedbackString, TextToSpeech.QUEUE_ADD, null);
						}
//						else if(toLocationID.equals("null"))
//							updateFeedback("Error: Invalid destination.");
//						else
//							updateFeedback("Error: No information.");
		
					}
		
		
		        }
		    }
		});
		
		
		
	}



}
