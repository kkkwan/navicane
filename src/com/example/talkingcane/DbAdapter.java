package com.example.talkingcane;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
import java.util.Random;

public class DbAdapter extends SQLiteOpenHelper{

	private static final String DATABASE_NAME = "navdb";
	private static final int DATABASE_VERSION = 1;
	private SQLiteDatabase mDb;
	private Context mContext;
	
	public DbAdapter(Context ctx){
		super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
		mContext = ctx;
		this.mDb = getWritableDatabase();
	}
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		
		String buildingsTable = "CREATE TABLE buildings (" +
									"id INTEGER PRIMARY KEY AUTOINCREMENT, " +
									"abbr TEXT NOT NULL, " +
									"name TEXT NOT NULL, " +
									"layout TEXT NOT NULL, " +
									"map_rotation REAL NOT NULL" +
								");";
		String rfidsTable = "CREATE TABLE rfid_t (" +
									"id TEXT PRIMARY KEY, " +
									"location TEXT NOT NULL, " +
									"building_id INTEGER, " +
									"FOREIGN KEY(building_id) REFERENCES buildings(id)" +
								");";
		String point2pointTable = "CREATE TABLE point_to_point (" +
									"from_id TEXT NOT NULL, " +
									"to_id TEXT NOT NULL, " +
									"desc TEXT NOT NULL, " +
									"face TEXT NOT NULL, " +
									"directions TEXT NOT NULL" +
								");";
		
		
			
		
		db.execSQL(buildingsTable);
		db.execSQL(rfidsTable);
		db.execSQL(point2pointTable);
				
		// populate database
		try {
//			BufferedReader in = new BufferedReader(new InputStreamReader(mContext.getAssets().open(FILE_NAME)));
			BufferedReader in = new BufferedReader(new InputStreamReader(mContext.getAssets().open("buildings.csv")));
			String line;
			
			while((line=in.readLine())!=null) {
				String fields[] = line.split("\\s*,\\s*");
				ContentValues values = new ContentValues();
			
				values.put("id", fields[0].replace("\"", ""));
				values.put("abbr", fields[1].replace("\"", ""));
				values.put("name", fields[2].replace("\"", ""));
				values.put("layout", fields[3].replace("\"", ""));
				values.put("map_rotation", fields[4].replace("\"", ""));
				db.insert("buildings", null, values); 
			}
			
			in = new BufferedReader(new InputStreamReader(mContext.getAssets().open("rfid_t.csv")));
			
			while((line=in.readLine())!=null) {
				String fields[] = line.split("\\s*,\\s*");
				ContentValues values = new ContentValues();
				values.put("id", fields[0].replace("\"", ""));
				values.put("location", fields[1].replace("\"", ""));
				values.put("building_id", fields[2].replace("\"", ""));
				db.insert("rfid_t", null, values); 
			}
			
			in = new BufferedReader(new InputStreamReader(mContext.getAssets().open("p_to_p.csv")));
			while((line=in.readLine())!=null) {
				String fields[] = line.split("\\s*,\\s*");
				ContentValues values = new ContentValues();
				values.put("from_id", fields[0].replace("\"", ""));
				values.put("to_id", fields[1].replace("\"", ""));
				values.put("desc", fields[2].replace("\"", ""));
				values.put("face", fields[3].replace("\"", ""));
				values.put("directions", fields[4].replace("\"", ""));
				db.insert("point_to_point", null, values); 
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
		db.execSQL("DROP TABLE IF EXISTS buildings");
		db.execSQL("DROP TABLE IF EXISTS rfid_t");
		db.execSQL("DROP TABLE IF EXISTS point_to_point");
		onCreate(db);
	}
	
	
	public Cursor fetchAll() {
		//table, columns, selection, selection args, group by, having, order by
		return mDb.query("buildings", new String[] {"abbr"}, null, null, null, null, null);
	}
	
	public Cursor getBuildingID(String bsearch)
	{
		return mDb.rawQuery("SELECT buildings.id " +
							"FROM buildings " +
							"WHERE buildings.name = ? COLLATE NOCASE " +
									"OR buildings.abbr = ? COLLATE NOCASE ", new String[] {bsearch, bsearch});

	}
	
	public Cursor getBuildingLayout(String bsearch)
	{
		return mDb.rawQuery("SELECT buildings.layout " +
							"FROM buildings " +
							"WHERE buildings.name = ? COLLATE NOCASE " +
									"OR buildings.abbr = ? COLLATE NOCASE ", new String[] {bsearch, bsearch});

	}
	
	public Cursor getBuildingRotation(String bsearch)
	{
		return mDb.rawQuery("SELECT buildings.map_rotation " +
							"FROM buildings " +
							"WHERE buildings.name = ? COLLATE NOCASE " +
									"OR buildings.abbr = ? COLLATE NOCASE ", new String[] {bsearch, bsearch});

	}
	
	public Cursor getBuildingIDFromName(String bname)
	{
		return mDb.rawQuery("SELECT buildings.id " +
							"FROM buildings " +
							"WHERE buildings.name = ? COLLATE NOCASE", new String[] {bname});

	}
	
	public Cursor getBuildingIDFromAbbr(String babbr)
	{
		return mDb.rawQuery("SELECT buildings.id " +
							"FROM buildings " +
							"WHERE buildings.abbr = ? COLLATE NOCASE", new String[] {babbr});
	}
	
	public Cursor getBuildingIDFromRFID(String rfid_key)
	{
		Cursor cur;
		cur =  mDb.rawQuery("SELECT rfid_t.building_id " +
						"FROM rfid_t " +
						"WHERE rfid_t.id = ?", new String[] {rfid_key});
		return cur;
	}
	
	public Cursor findLocIDInBuilding(String lname, int bid)
	{
		return mDb.rawQuery("SELECT rfid_t.id " +
							"FROM rfid_t " +
							"JOIN buildings ON buildings.id = rfid_t.building_id " +
							"WHERE rfid_t.location = ? COLLATE NOCASE " +
							"AND buildings.id = " + bid, new String[] {lname});
	}
	
	public Cursor findLocNameInBuilding(String lid, int bid)
	{
		return mDb.rawQuery("SELECT rfid_t.location " +
							"FROM rfid_t " +
							"JOIN buildings ON buildings.id = rfid_t.building_id " +
							"WHERE rfid_t.id = ? COLLATE NOCASE " +
							"AND buildings.id = " + bid, new String[] {lid});
	}
	
	public Cursor getWhereToFace(String from_id, String to_id)
	{
		Cursor cur;
		cur =  mDb.rawQuery("SELECT point_to_point.face " +
						"FROM point_to_point " +
						"WHERE point_to_point.from_id = ? AND point_to_point.to_id = ?", new String[] {from_id, to_id});
		return cur;
	}
	
	public Cursor getDirections(String from_id, String to_id)
	{
		Cursor cur;
		cur =  mDb.rawQuery("SELECT point_to_point.directions " +
						"FROM point_to_point " +
						"WHERE point_to_point.from_id = ? AND point_to_point.to_id = ?", new String[] {from_id, to_id});
		return cur;
	}
	
	public Cursor getLocNameFromRFID(String lid)
	{
		return mDb.rawQuery("SELECT rfid_t.location " +
							"FROM rfid_t " +
							"WHERE rfid_t.id = ? COLLATE NOCASE ", new String[] {lid});
	}
	
}
